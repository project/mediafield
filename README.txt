Media Field
-----------
Currently consists of two media fields: audio field and video field.
Both of them were developed for CCK module and depend on it.

At the moment these fields use multimediafile.inc file with common functions.
It is planned to move them into filefield.module in the future.


Installing
----------

1. Put this package into your Drupal modules directory.

2. Download and enabled the Getid3 module http://drupal.org/project/getid3 
   remembering to install the getID3() PHP library from http://www.getid3.org/ 

3. Enable audiofield.module or videofield.module on 'admin/build/modules' page
   (CCK section) depending on what exactly fields you need.

4. Go to 'admin/content/types' page, choose a desired content type, click edit
   and add audio or video field.


Created by ARDAS group <info AT ardas DOT dp DOT ua>

Maintained by Openly Connected <info AT openlyconnected DOT com>